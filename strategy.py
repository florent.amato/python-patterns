"""
Implementation of the strategy pattern

*What is this pattern about?

*TL;DR
Enable selecting an algorithm at runtime
"""


class Order:

    def __init__(self, price, discount_strategy=None):
        self.price = price
        self.discount_strategy = discount_strategy
        print(self.__repr__())

    def price_after_discount(self):
        if self.discount_strategy:
            discount = self.discount_strategy(self)
        else:
            print("No discount applied")
            discount = 0

        new_price = self.price - discount

        return new_price

    def __repr__(self):
        fmt = "<Price: {}, price after discount: {}>"
        return fmt.format(self.price, self.price_after_discount())


def ten_percent_discount(order):
    print("Apply a ten percent discount")
    return order.price * 0.10


def on_sale_discount(order):
    print("Apply on sale discount")
    return order.price * 0.25 + 20


def main():
    Order(100)

    Order(100, discount_strategy=ten_percent_discount)

    Order(1000, discount_strategy=on_sale_discount)


if __name__ == "__main__":
    main()
